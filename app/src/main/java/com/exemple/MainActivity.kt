package com.exemple

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.exemple.data.DataManager
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity() {
    val dataManager by inject<DataManager>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dataManager.getPosts{data, error ->
            when{
                error != null -> txtResponse?.text = "afficher erreur"
                data != null -> txtResponse?.text = data.toString()
             }
        }
    }
}
