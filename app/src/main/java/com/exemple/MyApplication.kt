package com.exemple

import android.app.Application
import android.content.Context
import com.exemple.di.mainModule
import com.exemple.di.remoteModule
import org.koin.android.ext.android.startKoin

class MyApplication: Application() {


    init {
        instance = this
    }

    companion object {
        private var instance: MyApplication? = null
        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(mainModule) + remoteModule)
    }
}