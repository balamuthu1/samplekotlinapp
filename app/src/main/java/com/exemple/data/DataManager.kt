package com.exemple.data

import com.exemple.remote.SampleDriver
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

class DataManager: KoinComponent {
    val driver by inject<SampleDriver>()

    fun getPosts(completion: (Any?, Error?) -> Unit){
        driver.getPosts { any, error ->
            //save to DB and send completion
            //db.save(any)

            completion(any,error)
        }
    }
}