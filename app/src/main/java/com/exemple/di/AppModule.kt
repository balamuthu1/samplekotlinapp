package com.exemple.di

import com.exemple.data.DataManager
import org.koin.dsl.module.Module
import org.koin.dsl.module.module

val mainModule: Module = module {
    single { DataManager() }

}