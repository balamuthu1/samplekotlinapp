package com.exemple.di

import com.exemple.BuildConfig
import com.exemple.remote.ApiRequestService
import com.exemple.remote.GlobalInterceptor
import com.exemple.remote.SampleDriver
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

val remoteModule: Module = applicationContext {
    single { createOkHttpClient() }
    single { createWebService(get()) }
    single { SampleDriver() }
}

fun createOkHttpClient(): OkHttpClient {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    val globalInterceptor = GlobalInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

    val okHttpClientBuilder =
        OkHttpClient.Builder().addInterceptor(globalInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(httpLoggingInterceptor)
    return okHttpClientBuilder.build()
}

fun createWebService(okHttpClient: OkHttpClient): ApiRequestService {
    val gson = GsonBuilder()
        .setLenient()
        .create()

    val retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .baseUrl(BuildConfig.API_URL)
        .client(okHttpClient)
        .build()

    return retrofit.create(ApiRequestService::class.java)
}