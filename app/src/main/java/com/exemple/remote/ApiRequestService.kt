package com.exemple.remote

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ApiRequestService {
    @GET("posts/42")
    @Headers("Content-type: application/json")
    fun getSamplePost() : Call<Any>
}