package com.exemple.remote

import okhttp3.Interceptor
import okhttp3.Response
import org.koin.standalone.KoinComponent
import java.io.IOException
import java.net.URI

class GlobalInterceptor : Interceptor, KoinComponent {
    lateinit var uri : URI

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val mainRequest = chain.request()
        uri = mainRequest.url().uri()
        val mainResponse = chain.proceed(mainRequest)
        return when (mainResponse.code()) {
            in 200..299 -> mainResponse
            404         -> handleNotFoundError()
            else        -> handleNotFoundError()
        }
    }

    private fun handleNotFoundError(): Response{
        throw IOException("Not found", Error("404 not found"))
    }

}