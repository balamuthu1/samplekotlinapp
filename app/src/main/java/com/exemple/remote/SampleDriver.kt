package com.exemple.remote

import org.koin.standalone.KoinComponent
import org.koin.standalone.inject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SampleDriver : KoinComponent {
    private val service by inject<ApiRequestService>()

    fun getPosts(completion: (Any?, Error?) -> Unit){
        service.getSamplePost().enqueue(object : Callback<Any>{
            override fun onResponse(call: Call<Any>, response: Response<Any>?) {
                response?.let {response ->
                    completion(response.body(), null)
                }
            }
            override fun onFailure(call: Call<Any>, t: Throwable?) {
               completion(null, Error(t?.message ?: "TError getting post"))
            }
        })
    }
}